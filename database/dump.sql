--DADOS DO BANCO DE DADOS

--DADOS DE USUARIO PARA VALDIAÇÃO DE LOGIN
INSERT INTO USERS(name, email, password) VALUES('Administrador', 'administrador@vdp.com', '1234');

--NOTICIAS
INSERT INTO NEWS(title, content_news) VALUES('Cidade de SP tem aumento de 2.700% nos casos de dengue em 2019', 'O número de casos de dengue na cidade de São Paulo aumentou 2.700% no último ano, segundo dados da Secretaria Municipal de Saúde. Em 2018 foram registrados 586 casos de dengue. Já em 2019, o número de casos saltou para 16.815.

É a primeira vez que a dengue voltar a crescer na cidade desde 2015, quando a capital paulista enfrentou uma epidemia. Na ocasião, a cidade registrou 100 mil casos.

Além disso, em 2018, nenhuma pessoa morreu em decorrência da doença, diferentemente do ano passado, quando três pessoas morreram na capital paulista.

Segundo a Secretaria Municipal da Saúde, neste ano, foi confirmado apenas um caso de dengue na cidade de São Paulo.

Em 2018, foram registrados 15.708 casos de dengue nos municípios paulistas e 14 pessoas morreram; já no ano passado, o número subiu para 400.184 casos, crescimento de cerca de 2.400%, e 263 pessoas morreram por causa da dengue.

Em nota, a Secretaria Estadual de Saúde reforçou o alerta sobre a importância das atitudes preventivas para evitar a proliferação do mosquito. A pasta destacou que materiais descartáveis, pneus e calhas podem favorecer o acúmulo de água, tornando-se potenciais criadouros.');


INSERT INTO NEWS(title, content_news) VALUES('Ouvidoria das polícias de SP diz que homem preso por atear fogo em morador de rua na Zona Leste pode não ter cometido o crime', 'Dois depoimentos colhidos pela Ouvidoria das polícias de São Paulo questionam a hipótese de que o morador de rua Flausino Cândido Filho foi responsável por atear fogo e matar um catador de lixo que dormia na Mooca, na Zona Leste. Carlos Roberto Vieira da Silva, de 39 anos, morreu na manhã do dia 6 de janeiro, após sofrer queimaduras graves em 70% do corpo. Ele foi atingido por uma explosão enquanto dormia em uma calçada.

Flausino foi preso pela Polícia Civil na última quarta-feira (8) e, segundo os delegados responsáveis, assumiu ter matado Carlos Roberto. No entanto, as testemunhas dizem que suas características físicas não batem com a de um homem visto no local pouco antes da explosão.

Além disso, Flausino tem dificuldade de locomoção, e por isso não seria o homem que aparece correndo após o incêndio em uma imagem de câmera de segurança que foi usada pelos policiais na investigação, diz outra testemunha.

Benedito Mariano, ouvidor das polícias de São Paulo, enviou os depoimentos à Polícia Civil e pede que eles sejam anexados à investigação.

"Hoje, tem uma pessoa presa por um crime bárbaro, mas temos dois depoimentos que contradizem o perfil da pessoa que está presa, de modo que tem que continuar as investigações", afirma Benedito Mariano, ouvidor das polícias de São Paulo.

A Secretaria de Segurança Pública de São Paulo (SSP), responsável pela Polícia Civil, afirma que as informações passadas pela ouvidoria foram recebidas e estão sendo verificadas, mas ainda não há mudanças na investigação. A SSP diz ainda que está à disposição para ouvir testemunhas que tenham novas informações sobre o crime.

O Ministério Público diz que não vai se manifestar sobre o caso porque o inquérito policial ainda não foi concluído. Já o Conselho Estadual de Defesa dos Direitos da Pessoa Humana disse que soube dos novos depoimentos e que defende o aprofundamento da investigação para evitar dúvidas e contradições.

Novos depoimentos
Segundo o ouvidor, os novos depoimentos colhidos pela ouvidoria nesta semana suscitam dúvidas graves e devem ser levados em consideração na investigação.

"São dúvidas muito graves e por isso que nós, além da Polícia Civil, encaminhamos os dois termos para o Ministério Público, para análise tanto da Polícia Judiciária quanto do Ministério Público. É um caso que ainda está em aberto, a partir desses dois depoimentos, do meu ponto de vista", afirma Benedito Mariano.

O primeiro depoimento, colhido na segunda-feira (13), é de um homem que passava pela rua de carro minutos antes da explosão. Ele conta que viu uma pessoa na esquina com características físicas diferentes das de Flausino, que está preso. Menos de dois minutos depois de avistar esse homem, houve a explosão, segundo esta testemunha.

Já na terça-feira (14) foi ouvida uma mulher que também é moradora da região e disse que conhece há muitos anos o morador de rua que está preso. Segundo ela, Flausino tem problemas de locomoção e portanto não pode ser o homem que aparece correndo após o crime em imagens de câmeras de segurança. Essas imagens foram usadas pelos investigadores para chegar a Flausino.');

