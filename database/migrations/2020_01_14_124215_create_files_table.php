<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });

        // Schema::table('news', function(Blueprint $table){
        //     $table->foreign('file_id')->references('id')->on('files');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('news', function(Blueprint $table){
        //     $table->dropForeign(['file_id']);
        // });

        Schema::dropIfExists('files');
    }
}
