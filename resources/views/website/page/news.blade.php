@extends('website.template.master')
@section('content')

    <div class="container px-0">
        <img src="{{asset('/image/website/banner-principal.svg')}}" width="100%" alt="">            
    </div>
    <div style="min-height: 10rem">
        <div class="row">
            <div class="col-sm-12 mt-4">
                <img class="rounded float-right mt-4 ml-4 mb-2 mr-0" src="{{asset('/image/website/resimg.png')}}" style="width: 30rem" alt="">
                <h1 class="mb-3">{{$news->title}}</h1>
                <p class="text-justify ">
                    {{$news->content_news}}
                </p>
            </div>
        </div>
    </div>
    
@endsection