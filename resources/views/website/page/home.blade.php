@extends('website.template.master')
@section('content')

    <div class="container px-0">
        <img src="{{asset('/image/website/banner-principal.svg')}}" width="100%" alt="">            
    </div>
    
    <br>
    <h4>Todas as notícias</h4>
    <br>
    
    <div class="row" style="min-height: 10rem">
        @foreach ($news as $news)
        <div class="col-sm-4 mb-5">
            <div class="card">
                <img class="card-img-top" src="{{asset('/image/website/resimg.png')}}" alt="">
                <div class="card-body" style="height: 11rem">
                    <h5 class="card-title">{{Str::limit($news->title, 50)}}</h5>
                    <p class="card-text">{{Str::limit($news->content_news, 120)}}</p>
                </div>
                <a class="btn btn-dark m-3" href="/noticia/{{$news->id}}" role="button" style="max-width: 7rem">Ler mais...</a>
                <div class="card-footer">
                    <small class="text-muted">Postado em {{$news->created_at}}</small>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection


