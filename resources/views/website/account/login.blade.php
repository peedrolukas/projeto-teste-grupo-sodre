<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,400,700,900&display=swap" rel="stylesheet">  
    <link rel="stylesheet" href="{{ asset('css/website.css') }}">
</head>
<body>   
    <div class="login-background">
        <div>
            <img src="{{asset('image/website/logo-login.svg')}}">
        </div>
        <div class="login-wrapper">
            <form action="valida" method="POST">
                <h1>Seja bem vindo</h1>
                <P>Acesso restrito ao colunista</P>
                
                <input 
                    class="mt-20 input" 
                    type="email"
                    name="email" 
                    placeholder="E-mail"
                    required>

                <input 
                    class="input" 
                    type="password" 
                    name="password" 
                    placeholder="Senha" 
                    required>

                <button class="input button-primary">Login</button>            
            </form>
        </div>
    </div>
    <script src="{{ mix('js/website.js') }}"></script>  
</body>
</html>
