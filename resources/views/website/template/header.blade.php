<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="collapse navbar-collapse container" id="navbarHeader">
        {{-- Logo --}}
        <img src="{{ asset('image/website/vdp.svg') }}" height="30"/>
        {{-- Navegação  --}}
        <ul class="ml-3 navbar-nav mr-auto">
            <li class="nav-item"><a class="nav-link" href="{{route('home')}}">Home</a></li>
            <li class="nav-item disabled"><a class="nav-link" href="#">Sobre</a></li>
            <li class="nav-item disabled"><a class="nav-link" href="#">Notícias</a></li>
            <li class="nav-item disabled"><a class="nav-link" href="#">Contato</a></li>
        </ul>
        {{-- Botão login --}}
        <a class="btn btn-outline-light badge badge-pill px-3 py-2" href="{{route('adm.login')}}" role="button">Entrar</a>
    </div>
</nav>