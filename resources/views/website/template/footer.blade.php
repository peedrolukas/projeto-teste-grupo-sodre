<footer class="mt-5">
    <div class="container">
        <img src="{{ asset('image/website/footer-detail.svg') }}" class="align-self-center" width="100%" alt="">
    </div>
    <div class="bg-dark">
        <div class="container mt-2">
            <div class="row">
                <div class="col-xl-2 mb-4 mt-5">
                    <p><a class="text-light" href="{{route('home')}}">Home</a></p>
                    <p><a class="text-light" href="#!">Sobre a VDP</a></p>
                    <p><a class="text-light" href="#!">Notícias</a></p>
                    <p><a class="text-light" href="#!">Contato</a></p>
                </div>
                <div class="col-xl-2 mb-4 mt-5">
                    <p><a class="text-light" href="{{route('adm.login')}}">Area do colunista</a></p>
                    <p><a class="text-light" href="#!">Perfil</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>