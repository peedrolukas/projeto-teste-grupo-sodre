@extends('website.template.master')
@section('content')

@include('website.admin.dashboard')

<a class="btn btn-dark mb-2" href="{{route('adm.new')}}" role="button">Criar nova notícia</a>
<div class="row mt-2" style="min-height: 15rem">
    {{-- tabela --}}
    <div class="col-sm-12 text-center mb-2">
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col" style="width: 10rem">#ID</th>
                <th scope="col">Título</th>
                <th scope="col" style="width: 15rem">Data da Publicação</th>
                <th scope="col" style="width: 10rem">Visualizações</th>
                <th scope="col-sm" style="width: 15rem">Ação</th>
                </tr>
            </thead>
            <tbody>
                
                @foreach ($news as $news)
                    <tr>
                        <th scope="row">{{$news->id}}</th>
                        <td>{{Str::limit($news->title, 30)}}</td>
                        <td>{{$news->created_at}}</td>
                        <td>{{$news->views}}</td>
                        <td>
                            <a class="btn btn-dark" href="/adm/conteudo/edit/{{$news->id}}" role="button">Editar</a>
                            <a class="btn btn-dark" href="/adm/conteudo/delete/{{$news->id}}" role="button">Excluir</a>
                        </td>
                    </tr>           
                @endforeach

            </tbody>
        </table>
    </div> 
</div>

@endsection