@extends('website.template.master')
@section('content')

@include('website.admin.dashboard')

<form action="{{route('adm.conteudo')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div>
                <h2 class="mb-3">Criando nova notícia</h2>      
            </div>
            <input class="input-group form-control mb-2" type="text" name="tituloNoticia"
                placeholder="Título da notícia">
            <textarea class="form-control" id="textArea" rows="10" name="conteudoNoticia"
                placeholder="Conteúdo da notícia"></textarea>
            <button type="submit" class="btn btn-dark mt-2">Salvar</button>
            <a class="btn btn-dark mb-2 mt-3" href="{{route('adm.conteudo')}}" role="button">Sair</a>
        </div>
    </div>
</form>

@endsection