<div class="row">
    <div class="col-sm-6 mx-0 d-inline-flex">
        <div class="card border-dark mb-3 mt-5 p-3" style="width: 100%">
            <h5 class="card-title">Nome do Usuário</h5>
            <p class="card-text">Cargo do Usuário</p>
        </div>
    </div>
    <div class="col-sm-6 d-inline-flex text-center">
        <div class="card border-dark mb-3 mt-5 p-3">
            <p class="card-text">total de notícias publicadas</p>
            <h1 class="card-title " style="width: 15rem">{{$news->count()}}</H1>
        </div>
        <div class="card border-dark ml-2 mb-3 mt-5 p-3">
            <p class="card-text">total de visualizações</p>
            <H1 class="card-title" style="width: 15rem">{{$news->sum('views')}}</H1>
        </div>
    </div>
</div>