@extends('website.template.master')
@section('content')

@include('website.admin.dashboard')

<form action="/adm/conteudo/{{$news->id}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-sm-4">
            <h2 class="mb-5">Editando notícia</h2>
            <h6>ID: {{$news->id}}</h6>
            <h6>Data de publicação: {{$news->created_at}}</h6>
            <h6>Visualizações: {{$news->views}}</h6>            
        </div>
        <div class="col-sm-8">
        <input class="input-group form-control mb-2" type="text" name="tituloNoticia" value="{{$news->title}}">
            <textarea class="form-control" rows="10" name="conteudoNoticia">{{$news->content_news}}</textarea>
            <button type="submit" class="btn btn-dark mt-2">Salvar</button>
            <a class="btn btn-dark mb-2 mt-3" href="{{route('adm.conteudo')}}" role="button">Sair</a>
        </div>
    </div>
</form>

@endsection