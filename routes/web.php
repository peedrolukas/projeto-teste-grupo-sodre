<?php

Route::get('/', ['as' => 'home', 'uses' => 'Website\PagesController@home']);

Route::get('/noticia/{id}', ['as' => 'news', 'uses' => 'Website\PagesController@news']);

Route::group(['prefix' => 'adm', 'as' => 'adm.'], function () {
    Route::get('/login', ['as' => 'login', 'uses' => 'Website\UsersController@login']);
    Route::post('/valida', ['as' => 'valida', 'uses' => 'Website\UsersController@valida']);

    Route::get('/conteudo', ['as' => 'conteudo', 'uses' => 'Website\AdminController@index']);
    Route::post('/conteudo', ['as' => 'conteudo', 'uses' => 'Website\AdminController@store']);

    Route::get('/conteudo/new', ['as' => 'new', 'uses' => 'Website\AdminController@create']);
    Route::get('/conteudo/edit/{id}', ['as' => 'edit', 'uses' => 'Website\AdminController@edit']);
    Route::post('/conteudo/{id}', ['as' => 'edit', 'uses' => 'Website\AdminController@update']);
    Route::get('/conteudo/delete/{id}', ['as' => 'delete', 'uses' => 'Website\AdminController@destroy']);
});