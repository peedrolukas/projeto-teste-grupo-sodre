<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Support\Facades\Cache;

class PagesController extends Controller
{
    public function home(){
        $news = News::all();
        return view('website.page.home', compact('news'));
        // return view('website.page.home');
    }

    public function news($id){
        $news = News::find($id);
        
        if(isset($news)){  
        Cache::add($news, 'views');
        $news->views+=1;
        $news->save();
        return view('website.page.news', compact('news'));     
        }
        return redirect('/');
    }
}
