<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;

class AdminController extends Controller

{

    public function index()
    {
        $news = News::all();
        return view('website.admin.admin', compact('news'));
    }


    public function create()
    {
        $news = News::all();
        return view('website.admin.new', compact('news'));
    }

    public function store(Request $request)
    {
        $news = new News();
        $news->title = $request->input('tituloNoticia');
        $news->content_news = $request->input('conteudoNoticia');
        $news->save();
        return redirect('adm/conteudo');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $news = News::find($id);
        if (isset($news)){
            return view('website.admin.edit', compact('news'));
        }
        return redirect('adm/conteudo');
    }

    public function update(Request $request, $id)
    {
        $news = News::find($id);
        if (isset($news)){
            $news->title = $request->input('tituloNoticia');
            $news->content_news = $request->input('conteudoNoticia');
            $news->save();
        }
        return redirect('adm/conteudo');
    }

    public function destroy($id)
    {
        $news = News::find($id);
        if (isset($news)) {
            $news->delete();
        }
        return redirect('adm/conteudo');
    }
}
