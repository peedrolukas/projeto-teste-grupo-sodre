<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function login(){
        return view('website.account.login');
    }

    public function valida(Request $request){

        $array = $request->all();
        
        $dados = DB::connection("projeto")
                ->table("users")
                ->select("*")
                ->where("email", $array['email'])
                ->get();
        
        if( $array['password'] == $dados[0]->password ){
            return redirect()->route('adm.conteudo');
        }else{
            return redirect()->route('adm.login');
        }
    }
}
